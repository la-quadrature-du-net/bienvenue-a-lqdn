# Sympathisant·es

Ton aide est la bienvenue !

Tout d'abord, merci de ton intérêt pour LQDN et ses actions, c'est grâce à la solidarité de nos sympathisant·es que nous continuons de pouvoir mener nos combats.

Nous organisons diverses actions, il y a donc plein de façons de contribuer !

La première question à se poser est : qu'est ce qui te motive ?

Puis : combien de temps as-tu envie d'y consacrer ?

#### 2 minutes
- Diffuser nos articles
    - Depuis tes propres comptes, diffuser les articles  que l'on a publié dernièrement (tu peux aussi nous mentionner pour qu'on relaye :) )
    - Tu peux en plus relayer ce que l'on poste sur les réseaux sociaux ;
        - https://mamot.fr/@laquadrature
        - https://twitter.com/laquadrature
        - https://video.lqdn.fr

- Nous suivre sur les réseaux sociaux
- S'abonner à notre flux RSS
- Tu peux diffuser nos actions sur les réseaux sociaux, par mail ou autour de toi ; nous t'avons préparé quelques modèles de messages à diffuser ;

    La Quadrature du Net défend depuis 2008 un internet libéré de la censure et de la surveillance, tout en se battant aussi contre la répression et la technopolice qui s'installe... Pour en savoir plus, jetez un coup d'oeil sur https://LQDN.fr  <3

- Liste d'images à relayer sur les réseaux :
    - Image de bannière
    - Image format carré
    - Emojo pour les instances Mastodon
    - Liens vers un kit de presse
    - Archive des stickers, que les gens puissent les réimprimer

#### 5 minutes

- Nous rejoindre sur les groupes de discussion
    - Tu peux nous rejoindre sur nos groupes de discussions IRC et Matrix ( Comment faire ? )
- Ajouter un article à la revue de presse
    - Une fois connecté·e au groupe IRC #lqdn-rp, tu peux ajouter un article à la revue de presse en postant un message de la forme suivante : !rp <lien vers ton article> . En savoir plus sur la Revue de Presse

#### 10 à 15 minutes

- Relire une traduction

- Alerter les décideurs et responsables politiques
    - Vous pouvez contacter les député·es, les sénateurs/trices et les eurodéputé·es pour leur faire part de vos inquiétudes et leur demander de soutenir ou de s'opposer à certains projets législatifs. Vous trouverez sur le site et sur le wiki toutes les informations nécessaires pour vous informer et trouver les coordonnées de vos élu·es.

- Contribuer aux outils informatiques
    - Si tu sais coder, tu peux te rendre sur git.laquadrature.net et demander un compte par mail à l'adresse sysadmin@laquadrature.net . Ensuite, tu peux relire du code des projets de la Quadrature ( https://git.laquadrature.net/ )

- Tu peux aussi signaler des anomalies sur les sites web en envoyant un rapport complet à l'adresse contact@laquadrature.net. Voici un message-type à utiliser pour nous signaler une anomalie ;

     **Message type**  
    _Sujet : Signalement d'anomalies_

    Bonjour,

    Je vous écris pour vous signaler une anomalie sur votre site web :

        Lien de la page :

        Type de bug : erreur graphique / mauvaise traduction / lien mort / bug du site / proposition d'amélioration  

        Screenshot si nécessaire :

        Description détaillée du soucis :

    // Fin

- Enfin, tu peux, depuis git.laquadrature.net, ouvrir un rapport de bug

#### 30 minutes

- Faire les sous-titres d'une vidéo LQDN
    - Vous pouvez commencer par sélectionner la vidéo qui vous intéresse sur video.lqdn.fr . Téléchargez-là, puis installez le logiciel Subtitle Composer ( https://subtitlecomposer.kde.org ) et ouvrez la vidéo ainsi téléchargée. Vous pouvez maintenant commencer à écrire vos sous-titres. Une fois fait, n'hésitez pas à faire relire vos sous-titres. Quand vous êtes content·e de votre sous-titrage, vous pouvez nous les envoyer par mail, en précisant bien pour quelle vidéo vous faites les sous-titres ( en donnant le lien en même temps par exemple ).
- Participer au tri des tickets sur Gitlab
    - Une fois votre compte créé sur git.laquadrature.net ( voir plus haut ), vous pouvez voir les différents projets de La Quadrature. Rendez-vous dans les tickets de ces projets, et ajoutez vos commentaires si vous pouvez apporter une information utile à la résolution du souci.

- Faire une affiche ou un graphisme pour LQDN
    - Que ça soit une illustration pour un article, un dessin, un design pour autocollant, ton imagination est la seule limite.

- Faire une demande CADA pour Technopolice
    - Voir cet article : https://technopolice.fr/blog/cadathon/ et celui-ci : https://technopolice.fr/blog/madada-exigeons-les-documents-de-la-technopolice/

- Afficher votre soutien sur le site
    - Si une organisation non gouvernementale à laquelle vous contribuez souhaite afficher son soutien à La Quadrature du Net, écrivez nous à soutien@laquadrature.net, en fournissant une description de la structure (français/anglais), un logo (si possible haute-définition), une adresse de contact, éventuellement une citation expliquant les raisons de votre soutien.

- J'aimerais m'impliquer plus régulièrement !
    - Avec joie ! Tu peux nous rejoindre sur notre forum : bbs.lqdn.fr . Une fois ton compte accepté, tu peux te présenter et rejoindre les membres, sympathisant·es et employé·es de La Quadrature dans notre lutte :)

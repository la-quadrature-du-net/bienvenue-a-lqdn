# SSO

L'interface d'accès au SSO est sur sso.lqdn.fr (y a un petit bouton « Sign In » en haut à droite). Sur cette interface, vous pouvez modifier vos informations de compte, votre nom, votre prénom, ou encore ajouter un OTP.

## Activation du compte SSO

Chaque @membres a un compte SSO de prêt. Pour l'activer, il faut simplement que vous demandiez une réinitialisation de votre mot de passe à partir de votre adresse email.

## Connexion à Nextcloud via le SSO

Sur cloud.lqdn.fr, vous avez un menu supplémentaire pour vous connecter avec le SSO. C'est assez intuitif je pense. :) __À noter que la connexion SSO ne fonctionne que sur cloud.lqdn.fr, pas sur cloud.laquadrature.net.__

Si vous synchronisez votre compte Nextcloud sur une application tierce (par exemple DAVx5 pour l'agenda) et que celle-ci ne gère pas la connexion SSO, il faut que vous créiez un « mot de passe application » ici : https://cloud.lqdn.fr/settings/user/security L'application Android Nextcloud sait gérer le SSO et il n'y a pas besoin de créer un tel mot de passe.

Si vous avez une erreur Nextcloud lors de la connexion, c'est peut-être parce que j'ai mal fait le rapprochement entre l'ancien compte Nextcloud et votre compte SSO. Pinguez-moi et je réparerai ça.

## Connexion à Matrix

L'accès à Matrix se fait via le SSO. Pas besoin de créer de compte supplémentaire. Lors de votre première connexion, le serveur vous demandera quelle adresse Matrix utiliser. Si vous avez un autre compte Matrix que vous comptez continuer à utiliser, vous n'avez pas besoin d'un compte sur le serveur de LQDN puisque le serveur LQDN se fédère très bien avec le reste du monde (d'autant plus que la plupart des clients Matrix ne gèrent pas le multicompte), donc qu'on pourra ajouter des comptes de membres qui ne sont pas sur le serveur LQDN pour nos futurs groupes de travail internes.

Pour celleux qui ne connaîtraient pas trop Matrix, il est important de vous rappeler que **vos clés privées de chiffrement doivent être sauvegardées en plus de votre mot de passe**. C'est pour cela que je vous recommande d'éviter au maximum une connexion via un navigateur web et de privilégier les clients lourds, pour éviter d'avoir à vous reconnecter souvent donc de devoir spécifier à nouveau la clé privée.

Le client Matrix le plus maintenu est [Element](https://matrix.org/docs/projects/client/element) (qui existe aussi en version web sur element.laquadrature.net), mais il y en a [beaucoup d'autres](https://matrix.org/clients/).

# Faire un stage à La Quadrature  

Bienvenue !

Tu commences donc un stage au sein de La Quadrature, et plus précisément au sein de son équipe "opé" pour opérationnelle, qui regroupe les salariées et stagiaires.

Ce stage peut se faire tout ou partie en télétravail, selon ce qui a été décidé, et sinon le travail se fait au "Garage", nos locaux.
Et bien sur même si le stage ne le prévoit pas au départ, tu peux faire du télétravail de temps en temps, en fonction de tes besoins.

Le canal de discussion principal de l'équipe opé, hors la vraie vie, c'est notre salon matrix : https://matrix.to/#/#ungarage:laquadrature.net

Lors du début de ton stage, l'équipe technique devra te créer des accès aux différents services de La Quadrature.

Tu peux commencer par explorer les différents services que l'on utilise au quotidien dans La Quadrature ;
- https://bbs.lqdn.fr : Notre forum interne, appelé BBS, où l'on discute de ce que l'on fait en ce moment,
- https://element.lqdn.fr : Notre service de tchat instantané, appelé Matrix, pour discuter avec les autres membres et salarié·es.
- https://git.laquadrature.net : Notre dépôt de code, qui sert aussi de Wiki pour certaines parties de LQDN, notamment ;
  - https://git.laquadrature.net/lqdn-interne/infrastructure-docs : Un dépôt où sont référencés tout les services et sites web de La Quadrature,
- https://cloud.lqdn.fr : Notre cloud, où l'on peut déposer des fichiers, gérer son agenda, etc...
- https://carre.lqdn.fr : L'ensemble des pads ( documents collaboratifs ), classé par dossier pour suivre les affaires en cours
- https://bbb.lqdn.fr : Notre service de visio conférence.
- Le Pont de Conf : Notre pont de conférence téléphonique, utilisé en parallèle du service de visio conférence.

Ensuite, on peut discuter de l'organisation du travail.

- Chaque semaine, nous faisons la _réu hebdo_, qui est une réunion où l'on fait un petit retour aux autres membres du groupe sur ce qu'il se passe en ce moment dans notre groupe de travail. Ce compte rendu est publié sur le BBS. Cette réunion a lieu, sauf changement, le mardi à 11h.
- Chaque fin de semaine, nous publions le QSPTAG ( prononcé cube-stague ), qui est une newsletter qui résume "Que Se Passe-T-il Au Garage ?".
- L'organisation et les horaires de travail sont assez souples, mais cela nécessite de prévenir le reste de l'équipe en cas d'absence ou de gros décalages par rapport aux "horaires de bureau", et de s'arranger pour avoir quand même des plages horaire qui recoupent celles du reste de l'équipe, ainsi que de participer aux réunions principales.

Tu peux commencer par te présenter sur le BBS, dans la catégorie [Présentation](https://bbs.lqdn.fr/category/4/pr%C3%A9sentations).
Et pour tout question, toute l'équipe est bien sur dispo, ne pas hésiter à demander (y'a pas de questions bêtes quand on arrive dans une nouvelle structure ;-) ).
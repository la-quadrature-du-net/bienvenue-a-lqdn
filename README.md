# Bienvenue � LQDN

> Le guide de ceux et celles qui millitent � La Quadrature du Net

Commencez par choisir votre r�le. Cliquez sur le r�le pour obtenir un guide �tape par �tape.

| R�e | Description |   
|---|---|
| [Sympathisant·e](sympathisant-e.md) | S'investir de temps en temps pour La Quadrature, sans conditions |  
| [Membre](membre.md) | S'investir r�guli�rement pour LQDN, apr�s avoir �t� sympathisant�e pendant un moment et co-opt� par les membres actuels |  
| [Salarier-�re](salariere.md) | Embauch� par La Quadrature |  
| [Stagiraire](stage.md) | Pour celles et ceux effectuant un stage �  La Quadrature |

----

## Tutoriels

Liste de tutoriels sur les proc�dures de travail au sein de LQDN, comment utiliser les outils num�riques...

- [Comment publier un article pour La Quadrature Du Net ?](tutoriels/comment-publier-un-article.md)

## Information diverse

- [Guide pour �crire en Markdown](https://www.markdownguide.org/cheat-sheet/)
    - Autant le forum, le Matrix, que le Gitlab utilisent un language de mise en page, nommm� Markdown ou MD qui est simple � apprendre et permet tout un tas de mise en page, et ce sans avoir besoin d'autre chose que d'un �diteur de texte.


## Contacts rapide

- [Contacter le groupe technique](matrix://#lqdn-sysadmin:laquadrature.net)
    - Le groupe technique est joignable sur Matrix, mais aussi par mail sur [sysadmin@laquadrature.net](mailto:sysadmin@laquadrature.net).

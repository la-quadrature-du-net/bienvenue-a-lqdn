<!-- Ceci est une copie du post BBS relatif à la création du Matrix -->

# Matrix

#### [matrix], c'est quoi ?

Matrix est un protocole fédéré, un peu comme XMPP, ou ActivityPub sur Mastodon, qui sert à faire de la discussion instantanée. Il permet notamment de chiffrer de bout en bout nos conversations, comme Signal, mais sans dépendre d'un seul serveur. [Nous contrôlons notre propre serveur Matrix.](https://git.laquadrature.net/lqdn-interne/equipe_technique/-/issues/18).

#### Quel intérêt pour La Quadrature ?

- Permettre de rassembler en un seul endroit nos discussions internes, ainsi que les discussion avec les sympatisant.es
- Avoir des discussion sécurisées pour les groupes internes, sans avoir besoin d'un téléphone, ni d'un nouveau compte.
- Avoir des discussions dans un format plus riche que sur IRC : images, audio, fichiers, emojis, réactions...
- Faire des ponts avec d'autres services de discussions ; à l'heure actuelle, on est connecté à IRC, mais à l'avenir, si besoin, on peut aussi se connecter à Discord, Email, SMS, XMPP, et plus encore.. Interop en action !

#### Comment on se connecte ?

Vous pouvez dès maintenant vous connecter !

- Allez sur https://element.laquadrature.net
- Continuez avec Keycloak
- Cliquez sur "Oublie du mot de passe"
- Mettez votre adresse mail laquadrature.net ou l'adresse que vous utilisez pour le Nextcloud à défaut. ( Sinon, pingez parlez en au groupe tech )
- Choissiez votre pseudo. Le choix du pseudo est libre, et n'est pas lié à vos comptes existants. Attention, ce choix est définitif.
- Vous voici sur Matrix !

> Si vous voulez rejoindre depuis votre téléphone, c'est possible aussi ! Element est disponible sur F-Droid !

Vous pouvez ensuite rejoindre les salons déjà existant ;

- [#laquadrature:laquadrature.net](https://matrix.to/#/#laquadrature:laquadrature.net?via=libera.chat&via=matrix.org&via=laquadrature.net) ( Pont avec IRC )
- [#lqdn-rp:laquadrature.net](https://matrix.to/#/#lqdn-rp:laquadrature.net) ( Pont avec IRC )  
- [#lqdn-travail:laquadrature.net](https://matrix.to/#/#lqdn-travail:laquadrature.net?via=laquadrature.net) ( pont avec IRC )

Il existe aussi des salons internes sur invitation. Vous devriez y être invités lors de votre arrivée.

---

Vous pouvez choisir d'autres clients pour Matrix ici : https://matrix.org/clients/

Et si vous le souhaitez, vous pouvez rejoindre les salons de LQDN depuis un autre serveur, sans soucis. Il suffit de le signaler , et on vous invitera !

# Membre

Bienvenue parmis les membres de La Quadrature ! :)

Une des premières choses à faire en étant membre, c'est d'obtenir les accès aux services internes de La Quadrature.

## Accès aux services internes

Lors de ton arrivée, le groupe tech doit t'avoir fait une adresse mail, ainsi qu'un compte LQDN. Ce compte, aussi appelé SSO ( Single Sign On) te permet de te connecter à l'ensemble des services de La Quadrature. Tu pourra bientôt retrouver l'ensemble des services disponibles sur le tableau de bord : https://tableau.lqdn.fr ( en construction début 2020 ).

Pour plus d'info sur les services existants ;

- [Matrix](matrix.md)
- [Le compte LQDN](sso.md)

Tu peux retrouver l'ensemble des services existants dans [le dépôt Gitlab de l'infrastructure](https://git.laquadrature.net/lqdn-interne/infrastructure-docs/-/tree/master/services-docs)

----

Une fois que cela est fait, tu peux commencer à t'invertir dans les groupes de travail.


## Groupes de travail

- Groupe Graphisme
- Groupe Tech
- Groupe Juridique
- Groupe Information, Archive et Presse
    - Se charge de suivre l'actualité pour alerter et information les autres groupes
    - Se charge de collecter des informations et d'archiver, notamment dans la [Bibliothèque sur le Nextcloud](https://cloud.lqdn.fr/f/987026), ainsi que celle du Garage et celle sur Inventaire.io.
    - Se charge de faire la Revue de Presse, où sont collectées les interventions de La Quadrature pour être mise en page et rediffusée.
- Groupe Éducation Populaire et Formation


Vous pouvez faire parti de plusieurs groupes de travail, et ces groupes de travail peuvent travailler sur des dossiers conjointements.
